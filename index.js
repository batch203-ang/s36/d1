const express = require("express");
const port = 4000;
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));

const mongoose = require("mongoose");
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.z2lte8d.mongodb.net/b203_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Connected to the cloud database"));


const taskRoutes = require("./routes/taskRoutes");
app.use("/tasks", taskRoutes);


app.listen(port, () => console.log(`Server is running at localhost:${port}`));