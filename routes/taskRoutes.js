const express = require("express");
const router = express.Router();

const taskControllers = require("../controllers/taskControllers");
console.log(taskControllers);

router.post("/", taskControllers.createTask);
router.get("/allTasks", taskControllers.getAllTasksController);
router.get("/getSingleTask/:taskId", taskControllers.getSingleTaskController);
router.patch("/updateTask/:taskId", taskControllers.updateTaskStatusController);
router.delete("/deleteTask/:taskId", taskControllers.deleteTaskController);

module.exports = router; 